import sys, os, logging, subprocess
from flask import Flask, render_template, jsonify, request

logging.basicConfig(level=logging.DEBUG)

def get_app_base_path():
    return os.path.dirname(os.path.realpath(__file__))

def get_instance_folder_path():
    return os.path.join(get_app_base_path(), 'instance')

app = Flask(__name__,
            instance_path=get_instance_folder_path(),
            instance_relative_config=True,
            template_folder='templates')
app.config.from_pyfile('config.cfg')
application = app  # for uwsgi to find when loading

@app.context_processor
def basic_template():
    return dict(headline=app.config['HEADLINE'], basepath=app.config['BASEPATH'])

@app.route('/')
def startpage():
    return render_template('index.html')

@app.route('/watch')
def watchpage():
    return render_template('watch.html')

@app.route('/handle/on_publish', methods=['POST'])
def startstream():
    """nginx calls this endpoint whenever it receives a PUBLISH on the submit application"""

    # Check authentication
    if request.form['name'] == app.config['STREAM_KEY']:
        resp = jsonify(success=True)
        return resp
    else:
        resp = jsonify(success=False)
        resp.status_code = 403
        return resp

@app.route('/handle/on_publish_done', methods=['POST'])
def stopstream():
    resp = jsonify(success=True)
    return resp