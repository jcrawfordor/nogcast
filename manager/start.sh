#!/bin/bash

mkdir -p /var/vid/hls || echo "HLS directory already exists"
mkdir -p /var/vid/dash || echo "Dash directory already exists"
mkdir -p /var/cache/nginx/ || echo "nginx cache dir already exists"

supervisord -c /etc/supervisor.d/supervisord.ini
